# -*- coding: utf-8 -*-
import math
from base_route import BaseRoute
import pandas as pd

__author__ = 'João André Prudêncio da Silva'

"""
 Essa clase refere-se ao arquivo tabela2/rotas.tsv e incluirá
 cálculo alfandegário.
"""


class CustomsRoute(BaseRoute):

    df_routes = pd.read_table('tabela2/rotas.tsv')
    df_price = pd.read_table('tabela2/preco_por_kg.tsv')

    def __init__(self, origem, destino, nota_fiscal, peso):
        """
        :param origem: Cidade de origem do frete
        :param destino: Cidade de destino do frete
        :param nota_fiscal: Valor do Produto
        :param peso: Peso do produto
        Inicia a clase e instancia o objeto self com os valores apropriados.
        """
        self.origem = origem
        self.destino = destino
        self.nota_fiscal = nota_fiscal
        self.peso = peso

        self.broken_limit = False

        # Demais atributos da classe
        # Routes Data Frame
        self.rdf_line = None

        # Price Data Frame
        self.pdf_line = None

        # Limite de peso
        self.limite = 0

        # Seguro
        self.insurance = 0

        # KG
        self.kg = ''

        self.preco_por_kg = 0

        # Total
        self.total = 0

        # ICMS
        self.icms = 0

        # Subtotal em relação ao peso
        self.total_val_kg = 0

        # Prazo de entrega
        self.prazo = 0

        # Taxa alfandegária
        self.alfandega = 0

        # Busca e seta os valores necessários que fazem parte do objeto
        self.activate()

    def get_line_routes_dataframe(self, origem, destino):
        """
         A linha correspondente ao par Origem/Destino
        """
        # Seleciona o conjunto de linhas cuja origem é igual a self.origem
        is_origem = self.df_routes.origem == origem

        # Seleciona o conjunto de linhas cuja origem é igual a self.origem
        is_destino = self.df_routes.destino == destino

        # Intersecção das consultas, resultando na linha correspondente ao par Origem/Destino
        line = self.df_routes[is_origem & is_destino]

        # Caso não se consiga satisfazer a condição
        if line.empty:
            print(u'Desculpe, a rota em questão não foi encontrada')
            self.rdf_line = None

        self.rdf_line = line

    def get_line_price_dataframe(self):
        """
        Resgata a linha correspondente a faixa e intervalo de peso
        """
        """
        Resgata a linha correspondente a faixa e intervalo de peso
        """
        for i, r in self.df_price.iterrows():
            if r['nome'] == self.kg and r['inicial'] <= self.peso:
                if r['final'] > self.peso or math.isnan(r['final']):
                    self.preco_por_kg = r['preco']
                    break

    def get_total_val_kg(self):
        """
        :return: O subtotal do frete em relação ao peso do produto
        """
        return float(self.preco_por_kg * self.peso)

    def get_val_insurance(self, nota_fiscal, seguro):
        """
        :param nota_fiscal: Valor da Nota Fisca
        :param seguro: Valor do seguro na tabela
        :return: o valor do total do seguro
        """
        return float(nota_fiscal * seguro / 100)

    def get_result(self):
        """
        :return: Output do programa
        """
        if self.broken_limit:
            return 'tabela2:-, -'
        return str('tabela2:' + str(self.prazo) + ', ' + str(self.total))

    def get_val_icms(self, subtotal):
        """
        :param subtotal: Subtotal do frete
        :return: valor total do frete com adição do ICMS
        """
        return float(subtotal / ((100 - self.icms) / 100))

    def get_val_freight(self):
        """
        :return: Valor total do frete
        """
        total = self.insurance + \
                self.total_val_kg

        total *= self.alfandega / 100 if self.alfandega > 0 else 1

        total = self.get_val_icms(total)

        return float("%.2f" % total)

    def activate(self):
        # Seta a linha correspondente ao par Origem/Destino
        self.get_line_routes_dataframe(self.origem, self.destino)

        self.limite = self.rdf_line.limite.values[0]

        if 0 < self.limite < self.peso:
            self.broken_limit = True
            self.get_result()
        else:
            # Seta o valor do seguro correspondente
            self.insurance = self.get_val_insurance(self.nota_fiscal, self.rdf_line.seguro.values[0])

            # Seta o nome da faixa usada para cobrança baseada no peso
            self.kg = self.rdf_line.kg.values[0]

            # Seta a linha correspondente ao kg/faixa de peso
            self.get_line_price_dataframe()

            # Valor do frete em relação ao peso do produto
            self.total_val_kg = self.get_total_val_kg()

            # Prazo de entrega
            self.prazo = self.rdf_line.prazo.values[0]

            # Valor do icms
            self.icms = self.rdf_line.icms.values[0]

            # Valor da taxa alfandegária
            self.alfandega = self.rdf_line.alfandega.values[0]

            # Total do frete com adição do imposto
            self.total = float("%.2f" % round(self.get_val_freight(), 2))