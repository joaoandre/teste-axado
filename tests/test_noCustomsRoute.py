# -*- coding: utf-8 -*-
from unittest import TestCase

__author__ = 'João André Prudêncio da Silva'

"""
 Classe de testes unitários para a classe NoCustomsRoute
"""
from no_customs_route import NoCustomsRoute
from customs_route import CustomsRoute


class TestRoute(TestCase):

    n_route = NoCustomsRoute('florianopolis', 'brasilia', 50, 7)
    c_route = CustomsRoute('florianopolis', 'brasilia', 50, 7)

    def test_get_val_insurance(self):
        self.assertEquals(self.n_route.insurance, 1.5)
        self.assertEquals(self.c_route.insurance, 1)

    def test_get_total_val_kg(self):
        self.assertEquals(self.n_route.get_total_val_kg(), 84)
        self.assertEquals(self.c_route.get_total_val_kg(), 101.5)

    def test_no_customs_route(self):
        self.assertEquals(self.n_route.get_result(), 'tabela:3, 104.79')
        self.assertEquals(self.c_route.get_result(), 'tabela2:2, 109.04')