# -*- coding: utf-8 -*-
import sys
from customs_route import CustomsRoute
from no_customs_route import NoCustomsRoute

__author__ = 'João André Prudêncio da Silva'

"""
    Assinatura
    -------
        Assinatura:         axado.py <origem> <destino> <nota_fiscal> <peso>
        Output por tabela:  <nome da pasta>:<prazo>, <frete calculado>

        Exemplo de output:

            $ axado.py florianopolis brasilia 50 7
            tabela:3, 104.79
            tabela2:2, 109.05

    Cálculo
    -------
        - Os preços devem ser arredondados para duas casa decimais, sempre para cima.
        - As colunas dos csvs definem as taxas que devem ser cobradas, as taxas/colunas
              devem ser calculadas da seguinte maneira:

                - seguro    = valor da nota fiscal * seguro / 100
                - fixa      = o próprio valor da taxa
                - kg        = define o nome da faixa que é usada para cobrar por kilograma,
                              a faixa é definida no arquivo `preco_por_kg.csv`
                - ICMS      = valor fixo definido no arquivo `rotas.csv` que deve ser
                              utilizado por último para calcular o total do frete:
                              TOTAL = SUBTOTAL / ((100 - icms) / 100)

        O ICMS deve sempre ser cobrado por último.

        Tabela1
        ----

            O ICMS dessa `tabela` é 6.

        Tabela2
        -------

            A tabela2 tem a taxa chamada "alfandega" que sempre deve ser calculada antes do
            ICMS usando a seguinte fórmula:

                subtotal * (alfandega / 100)

            A tabela2 também possui uma limitação de peso para alguns locais (definida na
            coluna limite do rotas.csv), indicando que não devem ser calculados os preços
            quando o peso ultrapassar esse limite:

                $ axado.py saopaulo florianopolis 50 130
                tabela:1, 1393.09
                tabela2:-, -

            Caso a rota não contenha nenhum valor limite, não há limites para a rota.
"""

if __name__ == '__main__':

    origem = sys.argv[1]
    destino = sys.argv[2]
    nota_fiscal = float(sys.argv[3])
    peso = float(sys.argv[4])

    n_route = NoCustomsRoute(origem, destino, nota_fiscal, peso)
    c_route = CustomsRoute(origem, destino, nota_fiscal, peso)

    print(n_route.get_result())
    print(c_route.get_result())
