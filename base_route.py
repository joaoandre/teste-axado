# -*- coding: utf-8 -*-
__author__ = 'João André Prudêncio da Silva'

from abc import ABCMeta, abstractmethod


class BaseRoute(metaclass=ABCMeta):
    """
     BaseRoute define uma classe abstrata que deve ser herdada
     pelas demais classe de Rota.
    """

    """
     Método abstrato para cálculo do seguro do frete .
    """
    @abstractmethod
    def get_val_insurance(self, val_nota_fiscal, val_seguro):
        pass

    """
     Método abstrato para cálculo do valor do ICMS.
    """
    @abstractmethod
    def get_val_icms(self, subtotal):
        pass

    """
     Método abstrato para cálculo valor total do frete.
    """
    @abstractmethod
    def get_val_freight(self):
        pass

    @abstractmethod
    def get_result(self):
        pass