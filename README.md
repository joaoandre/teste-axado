# README #

Teste para programador Python da Empresa Axado

### O que necessito? ###

Para instalar as dependências do projeto siga os passos abaixo dentro da pasta raiz do projeto (necessita do pacote virtualenv):

* '$ virtualenv venv_axado'
* '$ . venv_axado/bin/activate'
* '$ pip install -r requirements.txt'



### Como executar o programa? ###

Ainda com o virtualenv ativo, siga de acordo com as guidelines do teste:

* '$ python axado.py <origem> <destino> <nota_fiscal> <peso>'
