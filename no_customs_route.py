# -*- coding: utf-8 -*-
import math
from base_route import BaseRoute
import pandas as pd

__author__ = 'João André Prudêncio da Silva'

"""
 Essa classe refere-se ao arquivo tabela/routes.csv e não contém
 cálculo alfandegário.
"""


class NoCustomsRoute(BaseRoute):
    df_routes = pd.read_csv('tabela/rotas.csv')
    df_price = pd.read_csv('tabela/preco_por_kg.csv')

    def __init__(self, origem, destino, nota_fiscal, peso):
        """
        :param origem: Cidade de origem do frete
        :param destino: Cidade de destino do frete
        :param nota_fiscal: Valor do Produto
        :param peso: Peso do produto
        Inicia a clase e instancia o objeto self com os valores apropriados.
        """
        self.origem = origem
        self.destino = destino
        self.nota_fiscal = nota_fiscal
        self.peso = peso

        # Demais atributos da classe
        # Routes Data Frame
        self.rdf_line = None

        # Seguro
        self.insurance = 0

        # Taxa
        self.fixa = 0

        # KG
        self.kg = ''

        self.preco_por_kg = 0

        # Total
        self.total = 0

        # ICMS
        self.icms = 6

        # Subtotal em relação ao peso
        self.total_val_kg = 0

        # Prazo de entrega
        self.prazo = 0

        # Busca e seta os valores necessários que fazem parte do objeto
        self.activate()

    def get_line_routes_dataframe(self, origem, destino):
        """
         A linha correspondente ao par Origem/Destino
        """
        # Seleciona o conjunto de linhas cuja origem é igual a self.origem
        is_origem = self.df_routes.origem == origem

        # Seleciona o conjunto de linhas cuja origem é igual a self.origem
        is_destino = self.df_routes.destino == destino

        # Intersecção das consultas, resultando na linha correspondente ao par Origem/Destino
        line = self.df_routes[is_origem & is_destino]

        # Caso não se consiga satisfazer a condição
        if line.empty:
            print(u'Desculpe, a rota em questão não foi encontrada')
            self.rdf_line = None

        self.rdf_line = line

    def get_line_price_dataframe(self):
        """
        Resgata a linha correspondente a faixa e intervalo de peso
        """
        for i, r in self.df_price.iterrows():
            if r['nome'] == self.kg and r['inicial'] <= self.peso:
                if r['final'] > self.peso or math.isnan(r['final']):
                    self.preco_por_kg = r['preco']
                    break

    def get_total_val_kg(self):
        """
        :return: O subtotal do frete em relação ao peso do produto
        """
        return self.preco_por_kg * self.peso

    def get_val_insurance(self, nota_fiscal, seguro):
        """
        :param nota_fiscal: Valor da Nota Fisca
        :param seguro: Valor do seguro na tabela
        :return: o valor do total do seguro
        """
        return nota_fiscal * seguro / 100

    def get_val_freight(self):
        """
        :return: Valor total do frete
        """
        total = self.insurance + \
                self.fixa + \
                self.total_val_kg
        total = self.get_val_icms(total)

        return round(total, 2)

    def get_val_icms(self, subtotal):
        """
        :param subtotal: Subtotal do frete
        :return: valor total do frete com adição do ICMS
        """
        return subtotal / ((100 - self.icms) / 100)

    def get_result(self):
        """
        :return: Output do programa
        """
        return str('tabela:' + str(self.prazo) + ', ' + str(self.total))

    def activate(self):
        # Seta a linha correspondente ao par Origem/Destino
        self.get_line_routes_dataframe(self.origem, self.destino)

        # Seta o valor do seguro correspondente
        self.insurance = self.get_val_insurance(self.nota_fiscal, self.rdf_line.seguro.values[0])

        # Seta o nome da faixa usada para cobrança baseada no peso
        self.kg = self.rdf_line.kg.values[0]

        # Seta a linha correspondente ao kg/faixa de peso
        self.get_line_price_dataframe()

        # Seta o valor da taxa
        self.fixa = self.rdf_line.fixa.values[0]

        # Valor do frete em relação ao peso do produto
        self.total_val_kg = self.get_total_val_kg()

        # Prazo de entrega
        self.prazo = self.rdf_line.prazo.values[0]

        # Total do frete com adição do imposto
        self.total = float("%.2f" % self.get_val_freight())


